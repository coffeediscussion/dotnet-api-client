﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the PNL model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "profitAndLoss")]
    public class ProfitAndLoss
    {
        public override string ToString()
        {
            return $"ProfitAndLoss[ accountName: {AccountName}, " +
                   $"accountNo: {AccountNo}, " +
                   $"accountType: {AccountType}, " +
                   $"balance: {Balance}, subFundIdPnl: {SubFundIdPnl}, " +
                   $"weightTNav: {WeightTNav} ]";
        }

        [DataMember(Name = "accountName")]
        public string AccountName { get; set; }

        [DataMember(Name = "accountNo")]
        public long AccountNo { get; set; }

        [DataMember(Name = "accountType")]
        public string AccountType { get; set; }

        [DataMember(Name = "balance")]
        public decimal Balance { get; set; }

        [DataMember(Name = "subFundIdPnl")]
        public string SubFundIdPnl { get; set; }

        [DataMember(Name = "weightTNAV")]
        public double WeightTNav { get; set; }
    }
}