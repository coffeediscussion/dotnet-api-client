﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the Balance Sheet model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "balanceSheet")]
    public class BalanceSheet
    {
        public override string ToString()
        {
            return $"BalanceSheet[ accountName: {AccountName}, accountNo: {AccountNo}, accountType: {AccountType}, " +
                   $"balance: {Balance}, subFundIdBalanceSheet: {SubFundIdBalanceSheet}, " +
                   $"weightTNav: {WeightTNav} ]";
        }

        [DataMember(Name = "accountName")]
        public string AccountName { get; set; }

        [DataMember(Name = "accountNo")]
        public string AccountNo { get; set; }

        [DataMember(Name = "accountType")]
        public string AccountType { get; set; }

        [DataMember(Name = "balance")]
        public decimal Balance { get; set; }

        [DataMember(Name = "subFundIdBalanceSheet")]
        public string SubFundIdBalanceSheet { get; set; }

        [DataMember(Name = "weightTNav")]
        public double WeightTNav { get; set; }
    }
}