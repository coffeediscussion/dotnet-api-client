﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the Portfolio model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "portfolios")]
    public class Portfolio
    {
        public override string ToString()
        {
            return $"Portfolio[ id: {Id}, clientId: {ClientId}, subFundId: {SubFundId}, subFundName: {SubFundName}, " +
                   $"subFundCurrency: {SubFundCurrency} ]";
        }

        [DataMember(Name = "aggregateBalanceSheetPacks")]
        public List<BalanceSheet> AggregateBalanceSheetPacks { get; set; }

        [DataMember(Name = "aggregatePositionPacks")]
        public List<Position> AggregatePositionPacks { get; set; }

        [DataMember(Name = "aggregateProfitAndLossPacks")]
        public List<ProfitAndLoss> AggregateProfitAndLossPacks { get; set; }

        [DataMember(Name = "aggregateTransactionPacks")]
        public List<Transaction> AggregateTransactionPacks { get; set; }

        [DataMember(Name = "changePercentTNav")]
        public decimal ChangePercentTNav { get; set; }

        [DataMember(Name = "clientId")]
        public string ClientId { get; set; }

        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "lastUpdated")]
        public string LastUpdated { get; set; }

        [DataMember(Name = "navDate")]
        public string NavDate { get; set; }

        [DataMember(Name = "payableBalanceSheet")]
        public decimal PayableBalanceSheet { get; set; }

        [DataMember(Name = "payBalSheetPercentToNetNav")]
        public decimal PayBalSheetPercentToNetNav { get; set; }

        [DataMember(Name = "recBalSheetPercentToNetNav")]
        public decimal RecBalSheetPercentToNetNav { get; set; }

        [DataMember(Name = "receivableBalanceSheet")]
        public decimal ReceivableBalanceSheet { get; set; }

        [DataMember(Name = "shareClasses")]
        public List<ShareClassPack> ShareClassPack { get; set; }

        [DataMember(Name = "subFundCurrency")]
        public string SubFundCurrency { get; set; }

        [DataMember(Name = "subFundId")]
        public string SubFundId { get; set; }

        [DataMember(Name = "subFundName")]
        public string SubFundName { get; set; }

        [DataMember(Name = "tNav")]
        public decimal TNav { get; set; }
    }
}