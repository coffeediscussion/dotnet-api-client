﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the Position model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "position")]
    public class Position
    {
        public override string ToString()
        {
            return $"Position[ positionName: {PositionName}, marketPrice: {MarketPrice}, " +
                   $"positionQuantity: {PositionQuantity}, positionCurrency: {PositionCurrency}, " +
                   $"positionType: {PositionType}, sedol: {Sedol} ]";
        }

        [DataMember(Name = "accrInterestsPositionCurrency")]
        public double? AccrInterestsPositionCurrency { get; set; }

        [DataMember(Name = "accrInterestsSubFundCurrency")]
        public double? AccrInterestsSubFundCurrency { get; set; }

        [DataMember(Name = "baseGainLoss")]
        public double? BaseGainLoss { get; set; }

        [DataMember(Name = "bookValue")]
        public double? BookValue { get; set; }

        [DataMember(Name = "bookValuePositionCurrency")]
        public double? BookValuePositionCurrency { get; set; }

        [DataMember(Name = "bookValueSubFundCurrency")]
        public double? BookValueSubFundCurrency { get; set; }

        [DataMember(Name = "brokerLongName")]
        public string BrokerLongName { get; set; }

        [DataMember(Name = "class2Name")]
        public string Class2Name { get; set; }

        [DataMember(Name = "class3Name")]
        public string Class3Name { get; set; }

        [DataMember(Name = "commission")]
        public double? Commission { get; set; }

        [DataMember(Name = "cusip")]
        public string Cusip { get; set; }

        [DataMember(Name = "discountRate")]
        public double? DiscountRate { get; set; }

        [DataMember(Name = "exchangeRate")]
        public double ExchangeRate { get; set; }

        [DataMember(Name = "expiryDate")]
        public string ExpiryDate { get; set; }

        [DataMember(Name = "fee")]
        public double? Fee { get; set; }

        [DataMember(Name = "fwdLongAmount")]
        public double? FwdLongAmount { get; set; }

        [DataMember(Name = "fwdLongCurrency")]
        public string FwdLongCurrency { get; set; }

        [DataMember(Name = "fwdShortAmount")]
        public double? FwdShortAmount { get; set; }

        [DataMember(Name = "fwdShortCurrency")]
        public string FwdShortCurrency { get; set; }

        [DataMember(Name = "interestRate")]
        public double? InterestRate { get; set; }

        [DataMember(Name = "internalPositionId")]
        public string InternalPositionId { get; set; }

        [DataMember(Name = "issueCountry")]
        public string IssueCountry { get; set; }

        [DataMember(Name = "localGainLoss")]
        public double? LocalGainLoss { get; set; }

        [DataMember(Name = "marketPrice")]
        public decimal MarketPrice { get; set; }

        [DataMember(Name = "marketValuePositionCurrency")]
        public decimal MarketValuePositionCurrency { get; set; }

        [DataMember(Name = "marketValueSubFundCurrency")]
        public decimal MarketValueSubFundCurrency { get; set; }

        [DataMember(Name = "maturityDate")]
        public string MaturityDate { get; set; }

        [DataMember(Name = "positionCurrency")]
        public string PositionCurrency { get; set; }

        [DataMember(Name = "positionName")]
        public string PositionName { get; set; }

        [DataMember(Name = "positionQuantity")]
        public decimal PositionQuantity { get; set; }

        [DataMember(Name = "positionType")]
        public string PositionType { get; set; }

        [DataMember(Name = "realized")]
        public double? Realized { get; set; }

        [DataMember(Name = "securityBic")]
        public string SecurityBic { get; set; }

        [DataMember(Name = "securityRic")]
        public string SecurityRic { get; set; }

        [DataMember(Name = "securitySubType")]
        public string SecuritySubType { get; set; }

        [DataMember(Name = "sedol")]
        public string Sedol { get; set; }

        [DataMember(Name = "settlementDate")]
        public string SettlementDate { get; set; }

        [DataMember(Name = "sourceAccountName")]
        public string SourceAccountName { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "strikeRate")]
        public double? StrikeRate { get; set; }

        [DataMember(Name = "subFundIdPosition")]
        public string SubFundIdPosition { get; set; }

        [DataMember(Name = "swapId")]
        public string SwapId { get; set; }

        [DataMember(Name = "tradeDate")]
        public string TradeDate { get; set; }

        [DataMember(Name = "unrealizedGainLossPercent")]
        public double? UnrealizedGainLossPercent { get; set; }

        [DataMember(Name = "unrealizedGainLossPositionCurrency")]
        public double? UnrealizedGainLossPositionCurrency { get; set; }

        [DataMember(Name = "unrealizedGainLossSubFundCurrency")]
        public double? UnrealizedGainLossSubFundCurrency { get; set; }

        [DataMember(Name = "weightTNav")]
        public double WeightTNav { get; set; }
    }
}