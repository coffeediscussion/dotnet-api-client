﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the Transaction model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "transaction")]
    public class Transaction
    {
        public override string ToString()
        {
            return $"Transaction[ transactionId: {TransactionId}, marketPrice: {MarketPrice}, " +
                   $"quantity: {Quantity}, positionCurrency: {PositionCurrency}, transactionType: {TransactionType} ]";
        }

        [DataMember(Name = "accruedInterestPositionCurrency")]
        public double? AccruedInterestPositionCurrency { get; set; }

        [DataMember(Name = "accruedInterestSubFundCurrency")]
        public double? AccruedInterestSubFundCurrency { get; set; }

        [DataMember(Name = "adjustmentType")]
        public string AdjustmentType { get; set; }

        [DataMember(Name = "baseProceeds")]
        public double? BaseProceeds { get; set; }

        [DataMember(Name = "bloombergId")]
        public string BloombergId { get; set; }

        [DataMember(Name = "bookValuePositionCurrency")]
        public double? BookValuePositionCurrency { get; set; }

        [DataMember(Name = "class2Name")]
        public string Class2Name { get; set; }

        [DataMember(Name = "class3Name")]
        public string Class3Name { get; set; }

        [DataMember(Name = "counterPartyId")]
        public string CounterPartyId { get; set; }

        [DataMember(Name = "counterPartyName")]
        public string CounterPartyName { get; set; }

        [DataMember(Name = "counterPartyShortName")]
        public string CounterPartyShortName { get; set; }

        [DataMember(Name = "cusip")]
        public string Cusip { get; set; }

        [DataMember(Name = "divIncomeBase")]
        public double? DivIncomeBase { get; set; }

        [DataMember(Name = "divIncomeLocal")]
        public double? DivIncomeLocal { get; set; }

        [DataMember(Name = "expiryDate")]
        public string ExpiryDate { get; set; }

        [DataMember(Name = "exRate")]
        public double? ExRate { get; set; }

        [DataMember(Name = "fundName")]
        public string FundName { get; set; }

        [DataMember(Name = "internalPositionId")]
        public string InternalPositionId { get; set; }

        [DataMember(Name = "localProceeds")]
        public double? LocalProceeds { get; set; }

        [DataMember(Name = "marketPrice")]
        public double? MarketPrice { get; set; }

        [DataMember(Name = "netDividendLocal")]
        public double? NetDividendLocal { get; set; }

        [DataMember(Name = "openCloseIndicator")]
        public string OpenCloseIndicator { get; set; }

        [DataMember(Name = "positionCurrency")]
        public string PositionCurrency { get; set; }

        [DataMember(Name = "positionName")]
        public string PositionName { get; set; }

        [DataMember(Name = "positionType")]
        public string PositionType { get; set; }

        [DataMember(Name = "quantity")]
        public double? Quantity { get; set; }

        [DataMember(Name = "realizedResultSubFundCurrency")]
        public double? RealizedResultSubFundCurrency { get; set; }

        [DataMember(Name = "reutersId")]
        public string ReutersId { get; set; }

        [DataMember(Name = "sedol")]
        public string Sedol { get; set; }

        [DataMember(Name = "settleAmountPositionCurrency")]
        public double? SettleAmountPositionCurrency { get; set; }

        [DataMember(Name = "settleAmountSubFundCurrency")]
        public double? SettleAmountSubFundCurrency { get; set; }

        [DataMember(Name = "settleCurrency")]
        public string SettleCurrency { get; set; }

        [DataMember(Name = "sourceAccountNumber")]
        public string SourceAccountNumber { get; set; }

        [DataMember(Name = "subFundIdTransaction")]
        public string SubFundIdTransaction { get; set; }

        [DataMember(Name = "tradeBrokerFees")]
        public double? TradeBrokerFees { get; set; }

        [DataMember(Name = "tradeCommissions")]
        public double? TradeCommissions { get; set; }

        [DataMember(Name = "tradeDate")]
        public string TradeDate { get; set; }

        [DataMember(Name = "transactionId")]
        public string TransactionId { get; set; }

        [DataMember(Name = "transactionType")]
        public string TransactionType { get; set; }

        [DataMember(Name = "valueDate")]
        public string ValueDate { get; set; }

        [DataMember(Name = "weightRealizedResultSubFundTNav")]
        public double? WeightRealizedResultSubFundTNav { get; set; }
    }
}