﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    [DataContract(Name = "token")]
    public class Token
    {
        [DataMember(Name = "access_token")] public string AccessToken { get; set; }
        [DataMember(Name = "expires_in")] public int ExpiresIn { get; set; } = 7199;
        [DataMember(Name = "token_type")] public string TokenType { get; set; } = "Bearer";
    }
}