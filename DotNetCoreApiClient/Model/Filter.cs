﻿using System;
using System.Collections.Generic;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents a set of attributes to filter the results.
    /// </summary>
    public class Filter
    {
        /// <summary>
        /// A nullable list of client codes. The result set will contain data for the intersection of the
        /// codes in the list with the codes owned by the user. For example, if the list contains <c>"123"</c>, <c>"234"</c> and the user
        /// owns <c>"234"</c>, <c>"456"</c>, only the data with client code <c>"234"</c> will be returned. If the list is null then data with all
        /// client codes owned by the user will be returned. If the list is empty no data will be returned (the intersection in
        /// this case will be empty).
        /// </summary>
        public List<string> Client { get; set; }

        /// <summary>
        /// A nullable list of portfolios/subfunds to return. The result set will contain data for the
        /// intersection of the  portfolios is the list with the portfolios that the current user is permitted to access. If the
        /// list is null all portfolios the user has access to will be returned. If the list is empty no data will be returned
        /// (the intersection in this case will be empty).
        /// </summary>
        public List<string> Portfolio { get; set; }

        /// <summary>
        /// A list of fields to exclude from the result set. If the list is <c>null</c> or empty all the fields
        /// will be returned.
        /// </summary>
        public List<string> Exclude { get; set; }

        /// <summary>
        /// The start date of the result set in the form of <c>yyyymmdd</c> string. If the start date is not set
        /// (<c>null</c>) then yesterday will be taken as a start date (*not last business day!*)
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// The end date (*inclusive*) of the result set in the form of <c>yyyymmdd</c> string. By default, if the
        /// end date is not set (<c>null</c>) it will be set to the start date.
        /// </summary>
        /// <remarks>Please note that the max date range is limited by 31 days!</remarks>
        public DateTime? EndDate { get; set; }
    }
}