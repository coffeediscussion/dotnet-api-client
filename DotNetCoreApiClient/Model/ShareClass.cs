﻿using System.Runtime.Serialization;

namespace DotNetCoreApiClient.Model
{
    /// <summary>
    /// The class represents the View Share Pack model. Please note this class is for demo purposes only and therefore
    /// it could be incomplete. Please refer to the Swagger page for up-to-date information.
    /// </summary>
    [DataContract(Name = "shareClasses")]
    public class ShareClassPack
    {
        public override string ToString()
        {
            return $"ShareClassPack[ shareClassId: {ShareClassId}, shareClass: {ShareClass}, shareClassCurrency: {ShareClassCurrency} ]";
        }

        [DataMember(Name = "changePercent")]
        public double? ChangePercent { get; set; }

        [DataMember(Name = "clientByShare")]
        public string ClientByShare { get; set; }

        [DataMember(Name = "differencePercent")]
        public double? DifferencePercent { get; set; }

        [DataMember(Name = "distributionFactor")]
        public double? DistributionFactor { get; set; }

        [DataMember(Name = "distributionFactorGains")]
        public double? DistributionFactorGains { get; set; }

        [DataMember(Name = "distributionFactorIncome")]
        public double? DistributionFactorIncome { get; set; }

        [DataMember(Name = "nav")]
        public double? Nav { get; set; }

        [DataMember(Name = "outstandingShares")]
        public double? OutstandingShares { get; set; }

        [DataMember(Name = "shareClass")]
        public string ShareClass { get; set; }

        [DataMember(Name = "shareClassCurrency")]
        public string ShareClassCurrency { get; set; }

        [DataMember(Name = "shareClassId")]
        public string ShareClassId { get; set; }

        [DataMember(Name = "tNav")]
        public double? TNav { get; set; }

        [DataMember(Name = "totalInflow")]
        public double? TotalInflow { get; set; }

        [DataMember(Name = "totalOutflow")]
        public double? TotalOutflow { get; set; }
    }
}