﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using DotNetCoreApiClient.Model;

namespace DotNetCoreApiClient.Services
{
    /// <summary>
    /// <para>The class represents one of the approaches in implementation of the interaction with the Data REST API.
    /// One of the key points here is the authentication (and authorization) process. Before any requests to the Data
    /// REST API the client application (service) must receive JWT token from the authentication/authorization service.
    /// This token must be provided with each and every request to the rest of the services (i.e. Fund Accounting service).
    /// The authentication process requires Client ID (aka App ID, Application ID) and Client Secret. The credentials
    /// are provided by RBC.</para>
    /// <para>The Data REST API are available for the clients in two environments: QA and PROD. The QA environment should be
    /// used for test and demo purposes. The PROD environment URL is provided by RBC for each client separately.</para>
    /// </summary>
    public class BaseService
    {
        /// <summary>
        /// Service host provided by the RBC One Data API team. In the test app we use QA environment.
        /// Please contact the RBC One Data API team for production environment information.
        /// </summary>
        private const string Host = "api-rbcone.rbcits.com";

        /// <summary>
        /// Root path for all endpoints
        /// </summary>
        private const string RootPath = "/secure/rbcone";

        /// <summary>
        /// URL scheme.
        /// </summary>
        private const string Scheme = "https";

        private static readonly HttpClient Client = new HttpClient();

        private static readonly Authorization Authorization = Authorization.Instance;

        private static Token Token => Authorization.Token;

        /// <summary>
        /// In this implementation we perform authorization on service creation. Therefore each instance of the service
        /// will have its own JWT token. This is OK for demo and test purposes. However for production it would be better
        /// to implement a separate authorization service that will be responsible to retrieval and renewal of the JWT token
        /// and sharing it across all other services.
        /// </summary>
        protected BaseService()
        {
        }

        /// <summary>
        /// The method creates a Throwable Observable if the response is not successful.
        /// </summary>
        /// <param name="responseMessage">HTTP response message with the status code and reason phrase</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>a Throwable Observable</returns>
        private static IObservable<T> Error<T>(HttpResponseMessage responseMessage)
        {
            return Observable.Throw<T>(
                new HttpRequestException(
                    $"{(int) responseMessage.StatusCode} ({responseMessage.ReasonPhrase})"));
        }

        /// <summary>
        /// <para>A utility method to send a GET request to the Data REST API. It builds an endpoint based on the RootPath,
        /// service path and query string. It also adds default headers to the request, including the acceptable
        /// media type and authorization header value (which is the JWT token received earlier).</para>
        /// </summary>
        /// <remarks>Please note that the current demo accepts JSON only. However the Data REST API services may provide data
        /// in other formats as well. Please refer to the Swagger web page for more details.</remarks>
        /// <param name="path">a service/endpoint path. The value will be concatenated with the RootPath defined above.</param>
        /// <param name="query">an optional query string.</param>
        /// <returns>an instance of Content Observable</returns>
        protected static IObservable<HttpContent> GetRequest(string path, string query = null)
        {
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Headers =
                {
                    Accept = {MediaTypeWithQualityHeaderValue.Parse("application/json")},
                    Authorization = new AuthenticationHeaderValue(Token.TokenType, Token.AccessToken)
                },
                RequestUri = Uri(path, query)
            };
            return SendMessage(httpRequestMessage);
        }

        /// <summary>
        /// <para>A utility method that asynchronously sends an HTTP request, checks the status of the response
        /// and returns content of the response, if the response is successful; otherwise it returns an HTTP-specific
        /// exception.</para>
        /// <para>The implementation is actively using Reactive Extensions as it is very well suited for async processes like
        /// interaction with REST services over HTTP.</para>  
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static IObservable<HttpContent> SendMessage(HttpRequestMessage message)
        {
            return Client.SendAsync(message).ToObservable()
                .SelectMany(responseMessage =>
                    responseMessage.IsSuccessStatusCode
                        ? Observable.Return(responseMessage.Content)
                        : Error<HttpContent>(responseMessage));
        }

        /// <summary>
        /// A utility method that builds an endpoint URI based on the scheme, environment-specific host (FQDN),
        /// Data REST API root path, endpoint-specific path and optional query string.
        /// </summary>
        /// <param name="path">an endpoint-specific path</param>
        /// <param name="query">an optional query string</param>
        /// <returns>an absolute URI that fully qualifies the endpoint.</returns>
        private static Uri Uri(string path, string query = null)
        {
            return new UriBuilder
            {
                Scheme = Scheme,
                Host = Host,
                Path = $"{RootPath}/{path}",
                Query = query
            }.Uri;
        }
    }
}