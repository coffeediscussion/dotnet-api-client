﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Runtime.Serialization.Json;
using DotNetCoreApiClient.Model;

namespace DotNetCoreApiClient.Services
{
    public static class NullableExtension
    {
        public static void Apply<T>(this T optional, Action<T> action) where T : class 
        {
            action(optional);
        }

        public static void IfPresent<T>(this T? optional, Action<T> action) where T : struct
        {
            if (optional.HasValue) action(optional.Value);
        }
    }

    /// <inheritdoc />
    /// <summary>
    /// <para>The class is a sample of domain-specific service implementation. This class represents Fund Accounting service.
    /// Please note that the class is for demo-purposes only and may be incomplete in terms of business functionality.
    /// The purpose of the class is to demo how the client's application/service can interact with the Data REST API.
    /// For full list of supported endpoints please refer to the Swagger web page.</para>
    /// <para>All Fund Accounting service methods that return a list of items support pagination
    /// and date range. 
    /// Even though the page and size parameters are optional on the Data REST API side we highly encourage you to
    /// make them mandatory on the client side. Considering the high volume of data, not specifying the parameters
    /// may cause interruptions and service quality degradation during the data transfer between the servers and
    /// clients.</para>
    /// </summary>
    public class FundAccountingService : BaseService
    {
        private const int DefaultPage = 0;
        private const int DefaultPageSize = 25;

        public Portfolio FindById(string id)
        {
            return GetRequest($"fa/v2/portfolios/{id}")
                .SelectMany(content => content.ReadAsStreamAsync())
                .Select(stream =>
                {
                    var serializer = new DataContractJsonSerializer(typeof(Portfolio));
                    return serializer.ReadObject(stream) as Portfolio;
                })
                .ToTask().Result;
        }

        /// <summary>
        /// Returns a summary list of portfolios limited by the filter and pagination.
        /// </summary>
        /// <param name="filter">query parameters to limit the result set. See <see cref="Filter">Filter</see> for more details.</param>
        /// <param name="page">a page of data to return</param>
        /// <param name="size">a size of the page</param>
        /// <returns>a list of the class instances</returns>
        public List<Portfolio> List(Filter filter, int page = DefaultPage, int size = DefaultPageSize)
        {
            var query = Query(filter, page, size);

            return GetRequest($"fa/v2/portfolios/", query)
                .SelectMany(content => content.ReadAsStreamAsync())
                .Select(stream =>
                {
                    var serializer = new DataContractJsonSerializer(typeof(List<Portfolio>));
                    return serializer.ReadObject(stream) as List<Portfolio>;
                })
                .ToTask().Result;
        }

        /// <summary>
        /// A generic method that retrieves portfolio-specific details such as balance sheets, positions, transactions,
        /// etc. It requests items from the Data REST API, converts them from JSON to a domain class and returns a list of the
        /// converted items.Please refer to the Swagger web page for full list of supported endpoints.
        /// </summary>
        /// <param name="id">an id of a specific record</param>
        /// <param name="entity">a details-specific portion of the endpoint (such as "transactions")</param>
        /// <param name="exclude">a list of fields to exclude</param>
        /// <typeparam name="T">the type of the domain class</typeparam>
        /// <returns>a list of the class instances</returns>
        private List<T> ListEntityById<T>(string id, string entity, params string[] exclude)
        {
            return GetRequest($"fa/v2/portfolios/{id}/{entity}",
                    string.Join("&", exclude.Select(field => $"exclude={field}")))
                .SelectMany(content => content.ReadAsStreamAsync())
                .Select(stream =>
                {
                    var serializer = new DataContractJsonSerializer(typeof(List<T>));
                    return serializer.ReadObject(stream) as List<T>;
                })
                .ToTask().Result;
        }

        public List<BalanceSheet> ListBalanceSheets(string id, params string[] exclude)
        {
            return ListEntityById<BalanceSheet>(id, "balancesheets", exclude);
        }

        public List<Position> ListPositions(string id, params string[] exclude)
        {
            return ListEntityById<Position>(id, "positions", exclude);
        }

        public List<ProfitAndLoss> ListProfitAndLosses(string id, params string[] exclude)
        {
            return ListEntityById<ProfitAndLoss>(id, "profitandlosses", exclude);
        }

        public List<ShareClassPack> ListShareClasses(string id, params string[] exclude)
        {
            return ListEntityById<ShareClassPack>(id, "shareclasses", exclude);
        }

        public List<Transaction> ListTransactions(string id, params string[] exclude)
        {
            return ListEntityById<Transaction>(id, "transactions", exclude);
        }

        /// <summary>
        /// <para>A utility method that builds a query string for the given parameters. </para>
        /// </summary>
        /// <param name="filter">query parameters to limit the result set. See <see cref="Filter">Filter</see> for more details.</param>
        /// <param name="page">a page to return</param>
        /// <param name="size">a size of the page</param>
        /// <returns>a query string</returns>
        private static string Query(Filter filter, int page, int size)
        {
            var query = new List<string>(new[] {$"page={page}", $"size={size}"});
            filter.Client?.ForEach(value => query.Add($"client={value}"));
            filter.Portfolio?.ForEach(value => query.Add($"portfolio={value}"));
            filter.Exclude?.ForEach(value => query.Add($"exclude={value}"));
            filter.StartDate.IfPresent(value => query.Add($"startDate={value:yyyyMMdd}"));
            filter.EndDate.IfPresent(value => query.Add($"endDate={value:yyyyMMdd}"));
            return string.Join("&", query);
        }
    }
}