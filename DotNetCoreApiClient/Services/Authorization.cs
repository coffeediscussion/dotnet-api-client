﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Json;
using DotNetCoreApiClient.Model;

namespace DotNetCoreApiClient.Services
{
    public sealed class Authorization
    {
        /// <summary>
        /// The client/app ID provided by the RBC One Data API team.
        /// </summary>
        private const string ClientId = "tiAHTNxkApXwgWBY9axJQnUGyqtkc5ia";

        /// <summary>
        /// The client secret provided by the RBC One Data API team.
        /// </summary>
        private const string ClientSecret = "649b1b5435838afb6f0bbb91ad97cb07b14277d4";

        /// <summary>
        /// Service host provided by the RBC One Data API team. In the test app we use QA environment.
        /// Please contact the RBC One Data API team for production environment information.
        /// </summary>
        private const string Host = "online.rbcis.com";

        /// <summary>
        /// URL scheme.
        /// </summary>
        private const string Scheme = "https";

        private static readonly HttpClient Client = new HttpClient();

        private static volatile Authorization _instance;
        private static readonly object Sync = new object();

        public static Authorization Instance
        {
            get
            {
                if (_instance != null) return _instance;
                lock (Sync)
                {
                    if (_instance == null)
                        _instance = new Authorization();
                }

                return _instance;
            }
        }

        private Token _token;

        public Token Token
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get => _token ?? (_token = Authorize());
        }

        private Authorization()
        {
        }

        /// <summary>
        /// <para>The method implements the process of authorization of the client app/service. Please note that the Client ID
        /// and Client Secret must be passed as application/x-www-form-urlencoded content.</para>
        /// </summary>
        private static Token Authorize()
        {
            var credentials = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                {"client_id", ClientId},
                {"client_secret", ClientSecret},
                {"grant_type", "client_credentials"},
                {"scope", ""}
            });

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = credentials,
                Headers =
                {
                    Accept = {MediaTypeWithQualityHeaderValue.Parse("application/json")},
                },
                RequestUri = Uri()
            };

            return SendMessage(httpRequestMessage)
                .Select(content =>
                {
                    switch (content.Headers.ContentType.MediaType)
                    {
                        case "application/json":
                        {
                            var serializer = new DataContractJsonSerializer(typeof(Token));
                            return serializer.ReadObject(content.ReadAsStreamAsync().Result) as Token;
                        }
                        case "text/plain":
                        {
                            var result = content.ReadAsStringAsync().Result;
                            var parts = result.Split(" ");
                            if (parts.Length != 2) throw new Exception($"Invalid token format: {result}");
                            return new Token() {AccessToken = parts[1]};
                        }
                            break;
                        default:
                            throw new Exception($"Unsupported media type: ${content.Headers.ContentType}");
                    }
                })
                .ToTask().Result;
        }

        private static IObservable<T> Error<T>(HttpResponseMessage responseMessage)
        {
            return Observable.Throw<T>(
                new HttpRequestException(
                    $"{(int) responseMessage.StatusCode} ({responseMessage.ReasonPhrase})"));
        }

        private static IObservable<HttpContent> SendMessage(HttpRequestMessage message)
        {
            return Client.SendAsync(message).ToObservable()
                .SelectMany(responseMessage =>
                    responseMessage.IsSuccessStatusCode
                        ? Observable.Return(responseMessage.Content)
                        : Error<HttpContent>(responseMessage));
        }

        private static Uri Uri()
        {
            return new UriBuilder
            {
                Scheme = Scheme,
                Host = Host,
                Path = "/apiproxy/auth"
            }.Uri;
        }
    }
}