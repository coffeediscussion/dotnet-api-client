﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetCoreApiClient.Model;
using DotNetCoreApiClient.Services;

namespace DotNetCoreApiClient
{
    internal static class Program
    {
        /// <summary>
        /// The purpose of the program is to demo interaction with the Data REST API. The code below is for
        /// demo purposes only.
        /// </summary>
        private static void Main(string[] args)
        {
            try
            {
                var service = new FundAccountingService();

                // List of all portfolios
                var filter = new Filter();
                var portfolios = service.List(filter, size: 10);
                portfolios.ForEach(Console.WriteLine);
                
                portfolios.FirstOrDefault()?.Apply(it =>
                {
                    service.FindById(it.Id).Apply(Console.WriteLine);

                    // List of balance sheets for the given portfolio
                    service.ListBalanceSheets(it.Id).ForEach(Console.WriteLine);

                    // List of PNLs for the given portfolio
                    service.ListProfitAndLosses(it.Id).ForEach(Console.WriteLine);

                    // List of positions for the given portfolio
                    service.ListPositions(it.Id, "positionName", "sedol").ForEach(Console.WriteLine);

                    service.ListShareClasses(it.Id).ForEach(Console.WriteLine);
                    
                    // List of transactions for the given portfolio
                    service.ListTransactions(it.Id).ForEach(Console.WriteLine);
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}