# .Net RBC One Data API Client 2.0
## Introduction
This is the home for the .Net Demo Client code for RBC One Data APIs. The demo teaches you how to implement a client 
application service for RBC One Data API in .NET Core and the C# language.
> The code in the repository is for demo purposes only.

> Version 1.0 is in release/v1 branch.

## Prerequisites
The code in the repository requires .Net Core 2.0 or higher. You�ll need to set up your machine to run .NET core.
You can find the installation instructions on the [.NET Core](https://www.microsoft.com/net/core) page. You can 
run this application on Windows, Linux, macOS or in a Docker container. You�ll need to install your favorite code 
editor. You can use whatever tools you are comfortable with.

We assume that you are familiar with the basics of the .NET Core Command Line Interface (CLI) and C# Language; able
to manage dependencies with NuGet; and know what is the JSON format.

## Dependencies
The code has the following dependencies:

- System.Runtime.Serialization.Json: Provides classes for serializing objects to the JavaScript Object Notation (JSON) and deserializing JSON data to objects.

- System.Reactive: Reactive Extensions (Rx) Main Library combining the interfaces, core, LINQ, and platform services libraries.

These dependencies can be installed with the NuGet tool.

## Getting Started
### Sign up for RBC One Data API
Before you are able to use your client in production you must sign up with RBC for RBC One Data API. During the process of
signing up you will receive client-specific application ID (aka Client ID) and secret (aka Client Secret). These credentials
are required for accessing the production data. 

Even though you will be able to develop and test your applications and services with the demo credentials, signing for the 
Data API gives you an access to additional development resources and technical support.

### Environments
The Data REST API are available for the clients in two environments: QA and PROD. The QA environment should be
used for test and demo purposes. The PROD environment URL is provided by the RBC team upon signing for the service.

### Authentication and Authorization
Data Security and Privacy at RBC is the highest priority. As an RBC client, you will benefit from a Data API architecture
built to meet the requirements of financial organizations.

As defined in RFC 7519, a JSON Web Token (JWT) is a safe way to represent a set of information between two parties - in 
our case between the server and the client.

In authentication, when the client successfully logs in using their credentials, a JSON Web Token will be returned and 
must be saved locally.

Whenever the client wants to access data, the client should send the JWT, in the Authorization header using the Bearer 
schema. This is a stateless authentication mechanism as the user state is never saved in server memory. The server's 
protected resources will check for a valid JWT in the Authorization header, and if it is present, the client will be 
allowed to access protected resources. As JWTs are self-contained, all the necessary information is there, reducing 
the need to query the database multiple times.

### Data API Client Implementation
For the demo implementation we chose .Net Core platform. This platform is a recommended one for  
cross-plaform high-performance scaleable containerizeable microservices 
([Choosing between .NET Core and .NET Framework for server apps](https://docs.microsoft.com/en-us/dotnet/standard/choosing-core-framework-server)).

Also the demo application heavily relies on Reactive Extensions. [Reactive Extensions](https://msdn.microsoft.com/en-us/library/hh242985(v=vs.103).aspx) 
(Rx) is a library for composing asynchronous
and event-based programs using observable sequences and LINQ-style query operators. This is an obvious choice
when need to deal with asynchronous communications over HTTP (RESTful Service Calls). 

Another important point to mention is that the code in this repository is *NOT* a reference implementation.
It is just *one of many* possible implementations of the interaction between the client and the Data REST API server. 
The demo application only highlights the key points that any implementation of a Data API client must have. 

In this application, we use the HttpClient class to make web requests to the RBC One Data API. Like all modern .NET 
APIs, HttpClient supports only async methods for its long-running APIs. To make the development easier we use 
Observables from the Reactive Extensions.

First step in our application is to get authorized by the server. We send a POST request to the authorization 
endpoint with the credentials to receive a JSON Web Token. In this application, we send an authentication request
for each instance of the client's services. For production version, it makes sense to create a separate 
Authentication and Authorization service that will be responsible for managing and sharing the JWT across all other 
services.

After the JWT has been received, we use HTTP GET requests to retrieve all the information we need. 

We use the JSON Serializer to convert JSON data into C# Objects. Our first task is to define C# classes to 
define the information we need from these responses. 

Then we create the base service with utility methods to send HTTP GET requests to the Data API server.

And finally we create a domain-specific service (such as FundAccountingService) to implement business specific logic.

You can find detailed information about the model and supported endpoints on the Swagger page in the RBC One Web Portal.

## Best Practices
### Limiting Amount of Transferred Data
#### Use pagination
Method ``list`` of the Fund Accounting service supports pagination. Even though the ``page`` and ``size`` 
parameters are optional on the Data REST API side we highly encourage you to make them mandatory on the client side.

#### Select only portfolios and client codes you need
Method ``list`` of the Fund Accounting service supports filtering by portfolios and client codes. If you know that
specific uses cases require subset of portfolios and/or client codes then it is recommended to include the subset only
into the filter.

#### Exclude not required attributes
All ``list`` methods of the Fund Accounting service supports field/attribute exclusion. If you know that
use cases don't need specific attributes then you should include these attributes into the filter.

#### Limit the date range
To decrease the amount of data and improve performance it is recommended to use as small date range as possible. 

### Required Attributes Only
The Data REST API provides an extensive data model. You don't need to replicate all the provided attributes
in your code. It is a good practice to model only attributes you need and omit everything else. It will decrease the
development and maintenance cost.